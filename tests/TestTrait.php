<?php

namespace App\Tests;

trait TestTrait
{
    public function getPrivateProperty($className, string $propertyName): \ReflectionProperty
    {
        $reflector = new \ReflectionClass($className);
        $property  = $reflector->getProperty($propertyName);
        $property->setAccessible(true);

        return $property;
    }

    public function getPrivateMethod($className, string $methodName): \ReflectionMethod
    {
        $reflector = new \ReflectionClass($className);
        $method    = $reflector->getMethod($methodName);
        $method->setAccessible(true);

        return $method;
    }
}
