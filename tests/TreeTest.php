<?php

namespace App\Tests;

use App\Tree;
use App\Tree\Node;
use PHPUnit\Framework\TestCase;

final class TreeTest extends TestCase
{
    use TestTrait;

    public function dataProviderTestNew(): array
    {
        return [
            [1, [new Node('Level 1, index 1')]],
            [3, [new Node('Level 1, index 1'), new Node('Level 2, index 1'), new Node('Level 2, index 2'), new Node('Level 3, index 1'), new Node('Level 3, index 2'), new Node('Level 3, index 3')]],
            [5, [new Node('Level 1, index 1'), new Node('Level 2, index 1'), new Node('Level 2, index 2'), new Node('Level 3, index 1'), new Node('Level 3, index 2'), new Node('Level 3, index 3'), new Node('Level 4, index 1'), new Node('Level 4, index 2'), new Node('Level 4, index 3'), new Node('Level 4, index 4'), new Node('Level 5, index 1'), new Node('Level 5, index 2'), new Node('Level 5, index 3'), new Node('Level 5, index 4'), new Node('Level 5, index 5')]],
        ];
    }

    /**
     * @dataProvider dataProviderTestNew
     */
    public function testNew(int $numLevels, array $expectedNodes): void
    {
        $tree = new Tree($numLevels);
        $this->assertEquals($numLevels, $this->getPrivateProperty($tree, 'numLevels')->getValue($tree));
        $this->assertEquals($expectedNodes, $this->getPrivateProperty($tree, 'nodes')->getValue($tree));
    }

    public function dataProviderTestIndex(): array
    {
        return [
            [1, 1, new Node('Level 1, index 1')],
            [2, 2, new Node('Level 2, index 2')],
            [4, 2, new Node('Level 4, index 2')],
            [5, 3, new Node('Level 5, index 3')],
        ];
    }

    /**
     * @dataProvider dataProviderTestIndex
     */
    public function testIndex(int $level, int $index, Node $node): void
    {
        $tree = new Tree(5);
        $this->assertEquals($node, $tree->index($level, $index));
    }
}
