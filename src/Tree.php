<?php

namespace App;

use App\Tree\Node;

final class Tree
{
    private $nodes = [];

    private $numLevels = 0;

    public function __construct(int $numLevels = 1)
    {
        if ($numLevels < 1) {
            throw new \InvalidArgumentException('Number of levels must be greater than or equals to 1');
        }

        $this->numLevels = $numLevels;

        for ($level = 1; $level <= $this->numLevels; ++$level) {
            for ($index = 1; $index <= $level; ++$index) {
                $this->nodes[] = new Node(sprintf('Level %s, index %s', $level, $index));
            }
        }
    }

    public function index(int $level, int $index): Node
    {
        $key = 0;
        for ($l = 1; $l <= $level; ++$l) {
            for ($i = 1; $i < $l; ++$i) {
                ++$key;
            }
        }
        $key += ($index - 1);

        if (!array_key_exists($key, $this->nodes)) {
            throw new \InvalidArgumentException('Invalid combination of level and index arguments');
        }

        return $this->nodes[$key];
    }
}
